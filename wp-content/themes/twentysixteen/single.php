<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<main id="main" class="site-main container" role="main">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
            <?php
		      // Start the loop.
                while ( have_posts() ) : the_post();

                    the_content();
                    // End of the loop.
                endwhile;?>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">
                <?php get_sidebar( 'sidebar-1' ); ?>
            </div>
        </div>
	</main><!-- .site-main -->
</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
