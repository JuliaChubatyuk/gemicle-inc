<?php get_header(); ?>
<div class="container">
    <div class="left-triangle"></div>
    <div class="right-triangle"></div>
    <div class="proom-background">
        <div class="proom-thumbnail">
            <div class="proom-logo"></div>
            <div class="btitle">EXPAND YOUR BUSINESS VIA CREATIVE SOLUTIONS!</div>
            <div class="stitle">We're focused on modern technology that powers progress and helps organizations to create and expand their activity in future. We provide sustainable development for companies in various fields of activity.</div>
        </div>
        <div class="proom-video-wrapper">
            <div class="proom-video-container">
                <div class="proom-video-inner">
                <div class="proom-title">Press room</div>
                <div class="video-wrapper">
                    <div id="fvideo" class="fvideo">
                        <iframe src="https://www.youtube.com/embed/1KzuIm-ZQJY?showinfo=0&rel=0&controls=0&autohide=1" width="420" height="240" frameborder="0"></iframe>
                        <div class="p-controller"></div>
                        <div class="title" id="fvideo-title"></div>
                    </div>
                    <div id="svideo" class="svideo">
                        <iframe src="https://www.youtube.com/embed/wxwK_fmUjLY?showinfo=0&rel=0&controls=0&autohide=1" width="420" height="240" frameborder="0"></iframe>
                        <div class="p-controller"></div>
                        <div class="title" id="fvideo-title"></div>
                    </div>
                    <div class="clear"></div>
                </div>
                <div class="proom-channels">
                    <div class="channel-title">Where we have been mentioned?</div>
                    <ul class="channels-list">
                        <a href="#"><li class="channel-item twoplustwo"></li></a>
                        <a href="#"><li class="channel-item kyiv"></li></a>
                        <a href="#"><li class="channel-item noviy"></li></a>
                        <a href="#"><li class="channel-item gate"></li></a>
                    </ul>
                </div>
            </div>
                </div>
        </div>
    </div>
    <div class="services-wrapper">
        <div class="services-background">
            <div class="services-title">Our succesful projects approve the high level of professionalism in providing the various services:</div>
            <div class="services-list">
                <div class="services-title-wrapper">
                    
                    <div class="consulting item">
                        <div class="title"><div class="bold-title">IT-consulting</div> improving the effiency of key business-process</div>
                        <div class="circle"></div>
                    </div>
                    <div class="outsorcing item">
                        <div class="title"><div class="bold-title">Outsorcing</div> cost cutting via contracting out of a business process to a third party</div>
                        <div class="circle"></div>
                    </div>
                    <div class="soft-dev item">
                        <div class="title"><div class="bold-title">Software development</div> creating products from scratch and their futher technical support</div>
                        <div class="circle"></div>
                    </div>
                    <div class="optimization item">
                        <div class="title"><div class="bold-title">Optimization</div> and reengineering  of existing applications</div>
                        <div class="circle"></div>
                    </div>
                    <div class="testing item">
                        <div class="title"><div class="bold-title">Software testing</div>QA automation, manual testing, performance testing, usability testing, security testing</div>
                        <div class="circle"></div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
</div>